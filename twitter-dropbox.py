from __future__ import print_function

import argparse
import datetime
import os
import shutil
import sys
import tempfile
from time import sleep

try:
    from ConfigParser import RawConfigParser
except ImportError:
    from configparser import RawConfigParser

try:
    import requests
    from bs4 import BeautifulSoup
except ImportError:
    print('Please ensure beautifulsoup4 and requests modules are installed!')
    exit(1)


class TwitterDropbox(object):

    def __init__(self, config_file):
        self.config_file = config_file
        if not os.path.exists(config_file):
            self.log('Configuration file %s not found!' % config_file)
            self.write_default_config()
            exit(1)

        self.config = RawConfigParser()
        self.config.read(self.config_file)
        self.logfile = self.config.get('twitter-dropbox', 'logfile')

        if self.config.get('twitter', 'username') == '':
            self.log('No twitter username set to monitor!')
            self.log('Edit %s and set a username in the [twitter] section.' %
                     self.logfile)
        self.twitter_feed_url = 'https://www.twitter.com/%s' %\
                                self.config.get('twitter', 'username')

        if self.config.get('twitter-dropbox', 'lock'):
            self.log('Configuration file %s is locked!' % self.config_file)
            self.log('If you think this is wrong, set locked to empty in %s.' %
                     self.config_file)
            exit(1)
        self.toggle_lock()

        if self.config.get('dropbox', 'access_token') != '':
            try:
                import dropbox
            except ImportError:
                self.log('Cannot access Dropbox without the dropbox module!')
                self.log('Please either install the dropbox module using pip')
                self.log('    or clear the dropbox access token in %s.' %
                         self.config_file)
                self.exit(1)
            else:
                self.log('Connecting to Dropbox API...')
                self.dropbox = dropbox.Dropbox(
                    self.config.get('dropbox', 'access_token'))
        else:
            self.dropbox = None

        if self.config.get('local', 'target_path') != '':
            self.local_target = os.path.abspath(
                self.config.get('local', 'target_path'))
            self.log('Local target path is %s.' % self.local_target)
            if not os.path.isdir(self.local_target):
                os.makedirs(self.local_target)

        self.tempdir = tempfile.mkdtemp()
        self.log('Temporary directory is %s.' % self.tempdir)

        try:
            int(self.config.get('twitter', 'latest_status'))
        except Exception:
            self.config.set('twitter', 'latest_status', 0)
            self.save_config()

    def get_timestamp(self, timestamp=None):
        if timestamp is None:
            timestamp = datetime.datetime.now()
        elif isinstance(timestamp, int):
            timestamp = datetime.datetime.fromtimestamp(timestamp)
        return timestamp.strftime('%Y-%m-%d %H.%M.%S')

    def log(self, message):
        stamped_message = '[%s] %s' % (self.get_timestamp(), message)
        with open(os.path.expanduser(self.logfile), 'a') as logfile:
            logfile.write(stamped_message)
        print(stamped_message)

    def save_config(self, config=None):
        if config is None:
            config = self.config
        with open(self.config_file, 'w') as config_file:
            config.write(config_file)

    def toggle_lock(self):
        if self.config.get('twitter-dropbox', 'lock'):
            lock_val = ''
        else:
            lock_val = 'locked'
        self.config.set('twitter-dropbox', 'lock', lock_val)
        self.save_config()

    def exit(self, code):
        self.log('Unlocking config file %s...' % self.config_file)
        self.toggle_lock()
        self.log('Clearing temporary directory %s...' % self.tempdir)
        shutil.rmtree(self.tempdir)
        self.log('Exiting.')
        exit(code)

    def write_default_config(self):
        self.log('Writing default configuration to %s...' %
                 self.config_file)
        default_config = RawConfigParser()
        default_config.add_section('twitter')
        default_config.add_section('dropbox')
        default_config.add_section('local')
        default_config.add_section('twitter-dropbox')
        default_config.set('twitter-dropbox', 'lock', '')
        default_config.set('twitter-dropbox', 'poll_freq', 60)
        default_config.set('twitter-dropbox', 'logfile',
                           os.path.expanduser('~/twitter-dropbox.log'))
        default_config.set('twitter', 'latest_status', 0)
        default_config.set('twitter', 'username', '')
        default_config.set('dropbox', 'access_token', '')
        default_config.set('dropbox', 'target_path', '/Screenshots/')
        default_config.set('local', 'target_path', '~/Pictures/')
        self.save_config(default_config)

    def get_images(self):
        self.log('Polling Twitter for new statuses...')
        timeline = requests.get(self.twitter_feed_url)
        timeline.raise_for_status()
        timeline_soup = BeautifulSoup(timeline.content, 'lxml')
        statuses = timeline_soup.find_all('div', {'class': 'tweet'})
        media_list = []
        for status in statuses:
            timestamp = int(status.find('span', {'class':
                                                 '_timestamp'})['data-time'])
            status_id = int(status['data-tweet-id'])
            if timestamp <= int(self.config.get('twitter', 'latest_status')):
                self.log('Got %s media links.' % len(media_list))
                break
            else:
                self.log('Got new status: %s' % status_id)
                media = status.find_all('div',
                                        {'class':
                                         'AdaptiveMedia-photoContainer'})
                media_list += [(timestamp, item['data-image-url'])
                               for item in media]
        self.config.set('twitter', 'latest_status', timestamp)
        self.save_config()
        return self.download_images(media_list)

    def download_images(self, media_list):
        self.log('Downloading %s media files from Twitter...' %
                 len(media_list))
        local_files = []
        for item in media_list:
            timestamp = item[0]
            url = item[1]
            filename = '%s' % (url.split('/')[-1])
            target_name = '%s_%s' % (self.get_timestamp(timestamp), filename)
            self.log('Getting %s from Twitter...' % filename)
            image = requests.get('%s:orig' % url)

            local_filename = os.path.join(self.tempdir, target_name)
            with open(local_filename, 'wb') as temp_file:
                temp_file.write(image.content)
                local_files.append(local_filename)
        return local_files

    def dropbox_upload(self, filename):
        target_path = self.config.get('dropbox', 'target_path')
        self.log('Uploading %s to Dropbox folder %s/...' %
                 (os.path.basename(filename), target_path))
        with open(filename, 'rb') as image:
            self.dropbox.files_upload(image.read(), '%s/%s' %
                                      (target_path,
                                       os.path.basename(filename)))

    def save_file(self, filename):
        self.log('Copying %s to %s/...' %
                 (os.path.basename(filename), self.local_target))
        shutil.copy(filename, self.local_target)

    def run(self):
        images = self.get_images()
        for image in images:
            if self.dropbox:
                self.dropbox_upload(image)
            if self.local_target:
                self.save_file(image)
            os.remove(image)
        self.log('Done!')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config',
                        default=os.path.expanduser('~/.twitter-dropbox.conf'),
                        help='A configuration file to run against.\
Will be created if it does not exist.')
    args = parser.parse_args()
    code = 0
    try:
        tdb = TwitterDropbox(args.config)
        while True:
            tdb.run()
            sleep(int(tdb.config.get('twitter-dropbox', 'poll_freq')))
    except Exception as exc:
        if isinstance(exc, KeyboardInterrupt):
            pass
        else:
            code = 1
            print(sys.exc_info())
    finally:
        tdb.exit(code)
