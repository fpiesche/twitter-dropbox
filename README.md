# Twitter Media Monitor

While running, this Python script will poll (by default every 60 seconds) a given Twitter account for new media and copy images to either a local directory or Dropbox (or both!).

I wrote this primarily in order to have a convenient way of getting game screenshots off of my PS4 and Switch - as these have Twitter integration but no other sensible way to download media off the console without the use of external storage devices or microSD card readers.

Someone on mastodon.social had the clever idea of setting up an alternative Twitter account exclusively for sharing screenshots to while keeping their "main" account tidy and just retweet things they did want to share with a wider audience, and I realised this would also be a good way of just getting screenshots off the consoles in general.

Twitter will sadly compress images again on upload so there is some quality loss versus copying the images directly off the consoles, but personally I'll accept this for the convenience of not having to mess around with USB sticks.


# Requirements

  * Python 2.7 or upwards
  * The beautifulsoup4 and requests Python modules
  * The dropbox Python module (only when using the Dropbox integration feature)

# Usage

The script runs mostly unattended. When you first run it, it will automatically create a configuration file in your home directory that you can edit to set up polling frequency, the account name to monitor, a log file to write status output to, and your Dropbox API access token (if you want to use Dropbox integration).

# Getting a Dropbox Access Token

If you want the script to automatically upload your images to Dropbox (e.g. if you're running the script on a server that is not your usual computer), you will need to create an access token and store this in the configuration file. In order to do this:

  * Visit https://www.dropbox.com/developers/apps and log in if you need to
  * Click `Create app`
  * Select `Dropbox API` as the API type
  * Select `Full Dropbox` as the access type
  * Enter a name, e.g. `<your name>'s Twitter Media Monitor`. Note that this must be a unique name across the entire Dropbox app ecosystem, but as it will not be public it's fairly arbitrary what you enter here.

Once the app is created, Dropbox should automatically take you to its Settings page; if it does not re-visit https://www.dropbox.com/developers/apps and log in and select the app you have just created under "My Apps". On this page:

  * Scroll to the `OAuth 2` section and click the `Generate access token` button.
  * Copy the token and paste it into your configuration file in the `access_token` entry in the `[dropbox]` section
